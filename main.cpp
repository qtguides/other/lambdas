#include <QCoreApplication>
#include <iostream>
#include <vector>


// 1. Puntero a funcion:
//    Se declara e implementa una funcion como siempre.
//    Se utiliza una variable puntero a funcion, y se la usa como una funcion.
int suma(int a, int b) {
    return a + b;
}
void usarPunteroFunction() {
    std::cout << std::endl << "usarPunteroFunction()" << std::endl;

    int(*operacion)(int x, int y) = suma;

    std::cout << "Suma Ptr-a-Func: " << operacion(5,3) << std::endl;
}

// 2. Puntero a funcion pero a un lambda:
//    La funcion se declara anonima dentro de otra funcion, en el momento de usarla.
//    Se utiliza una variable puntero a funcion, y se la usa como a una funcion.
void usarPunteroFunctionLambda() {

    std::cout << std::endl << "usarPunteroFunctionLambda()" << std::endl;

    // variable puntero a funcion lambda implementada en el momento de usarla.
    int(*operacion)(int x, int y) = [](int a, int b) {
        return a + b;
    };

    // Tambien es posible usar el keyword auto para no tener que declarar el tipo.
    auto mismaOperacion = [](int a, int b) {
        return a + b;
    };

    std::cout << "\tSuma Ptr-a-Lamda: " << operacion(5,3) << std::endl;
    std::cout << "\tSuma Ptr-a-Auto : " << mismaOperacion(5,3) << std::endl;
}

// 3. Uso de funciones lambda sin usar putneros a funcion:
//    [] () { ... }: sin acceso al contexto
//    [=]() { ... }: con acceso de solo lectura a todo el contexto (por valor)
//    [&]() { ... }: con acceso de total a todo el contexto (por referencia)
void lambdas1SinContexto() {
    std::cout << std::endl << "lambdas1SinContexto()" << std::endl;

    std::vector<int> v = { 1, 2, 3, 4, 5 };

    // Uso de una funcion lambda para aplicar una funcion implementada in-situ para cada elemento.
    //     [] Sin acceso al contexto.
    std::for_each(v.begin(), v.end(), [](int i) {
        std::cout << "\tv[ ] = " << i << std::endl;
    });
};
void lambdas2ContextoReadOnly() {
    std::cout << std::endl << "lambdas2ContextoReadOnly()" << std::endl;

    std::vector<int> v = { 1, 2, 3, 4, 5 };
    int a = 1, b = 2;

    // Uso de una funcion lambda para aplicar una funcion implementada in-situ para cada elemento.
    //     [a, b] Con acceso a dos variables de contexto, a y b, como solo lectura.
    std::for_each(v.begin(), v.end(), [a, b](int i) {
        std::cout << "\tv[ ] = " << i + a + b << std::endl;
    });

    // Uso de una funcion lambda para aplicar una funcion implementada in-situ para cada elemento.
    //     [=] Con acceso a TODAS las variables a y b como solo lectura.
    std::cout << std::endl;
    std::for_each(v.begin(), v.end(), [a, b](int i) {
        std::cout << "\tv[ ] = " << i + a + b << std::endl;
    });
}
void lambdas3ContextoFullAccess() {
    std::cout << std::endl << "lambdas3ContextoFullAccess()" << std::endl;

    std::vector<int> v = { 1, 2, 3, 4, 5 };
    int c = 0;

    // Uso de una funcion lambda para aplicar una funcion implementada in-situ para cada elemento.
    //     [&] Con acceso a TODAS las variables a y b como solo lectura.
    std::for_each(v.begin(), v.end(), [&](int i) {
        std::cout << "\tv[" << c << "] = " << i << std::endl;
        c++;
    });
}

// 4. Uso de funciones lambda:
//    sort
void lambdas4Uso() {
    std::cout << std::endl << "lambdas4Uso()" << std::endl;

    std::vector<int> v = { 1, 2, 3, 4, 5 };

    std::sort(v.begin(), v.end(), [](int a, int b) {
        return b < a; // para ordenar de mayor a menor
    });

    std::for_each(v.begin(), v.end(), [](int i) {
        std::cout << "\t" << i << std::endl;
    });
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    usarPunteroFunction();
    usarPunteroFunctionLambda();
    lambdas1SinContexto();
    lambdas2ContextoReadOnly();
    lambdas3ContextoFullAccess();
    lambdas4Uso();

    return a.exec();
}
